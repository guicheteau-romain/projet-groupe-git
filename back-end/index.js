const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express()
const port = 8080

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.route('/api').get((req,res)=>{
    fs.readFile('ingredient.json', 'utf8', function(err, data) {
        return res.status(200).json(JSON.parse(data));
      });
}).post((req,res)=>{
    const recette = JSON.parse(req.body.recette)

    //required values
    if (recette == '')
        return res.status(400).json({error:'recette is missing'})

    fs.readFile('ingredient.json', 'utf8', function(err, data) {
            const datas =JSON.parse(data)
            datas.push(recette)
            fs.writeFile('ingredient.json', JSON.stringify(datas), function (err) {
                if (err) throw err;
                return res.status(200).json({success:'the recipe is pushed'})
             });
      });    
})

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })


